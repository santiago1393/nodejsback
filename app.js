var http = require('http');
var mongo = require('mongodb');
var express    = require('express');       // Utilizaremos express, aqui lo mandamos llamar
var app        = express();                 // definimos la app usando express
var bodyParser = require('body-parser'); //

var { graphql, buildSchema, GraphQLList } = require('graphql');
var graphqlHTTP = require('express-graphql');

var faker = require('faker');

// configuramos la app para que use bodyParser(), esto nos dejara usar la informacion de los POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // seteamos el puerto
var router = express.Router();   //Creamos el router de express

// Seteamos la ruta principal
router.get('/faker', function(req, res) {
    res.json({ message: faker.fake("{{name.firstName}}")});
});

router.route('/orders').get(function(req,res){    
    readData().then( data => {
        console.log("Api datos");
        res.json(data);
    }).catch( error => {
        throw error;
    })
});

router.route('/orders').post(function(req,res){
    var order = {
        amount: req.param('amount'),
        cust_id: req.param("cust_id"),
        status: req.param("status")
    };    
    insertData(order).then( data => {
        if(data){
            res.json(data);
        }
    }).catch( error => {
        console.log(error);
        throw error;
    })
});

router.route('/orders').put(function(req,res){
    var query = { cust_id: req.param("cust_id") };
    var newData = { $set: {amount: req.param("amount"), status: req.param("status")}};
    console.log(query);
    console.log(newData);
    updateData(query, newData).then( data => {
        if(data){
            res.json(data);
        }
    }).catch( error => {
        console.log(error);
        throw error;
    })
});

router.route('/orders').delete(function(req,res){
    var query = { cust_id: req.param("cust_id") };  
    console.log(query);   
    deleteData(query).then( data => {
        if(data){
            res.json(data);
        }
    }).catch( error => {
        console.log(error);
        throw error;
    })
});



var schema = buildSchema(`
   type Query {
    getOrders: [Orders]    
  },
  type Orders{
      _id: String
      cust_id: String
      amount: Int
      status: String
  }  
`);

var root = { hello: () => 'Hello world!' };



var getOrders = { Orders: async () => { 
    const result = await readData();
    console.log("Ordenes");
    console.log(result);
    return result;
 }};

app.use('/graphql', graphqlHTTP({
    schema: schema,
    //fieldResolver: getOrders.Orders,  
    rootValue:{getOrders:getOrders.Orders},
    graphiql: true    
  }));

// Le decimos a la aplicación que utilize las rutas que agregamos
app.use('/api', router);

// Iniciamos el servidor
app.listen(port);
console.log('Aplicación creada en el puerto: ' + port);

var readData = function(){ 
    return new Promise((resolve,reject) =>{  
        conexionMongo().then(db => { 
            db.db("prueba").collection("orders").find(
                {},
                { projection:
                    {
                        _id: 1,
                        cust_id: 1,
                        amount: 1,
                        status:1
                    }
                }).toArray(
                    function(err,result){
                        db.close();
                        if(err){                           
                            reject(true);
                        }else{                            
                            resolve(result);
                        }
                    });
        }).catch(error => {
            console.log(error);
            reject(error);
        });    
    });       
}

var insertData = function(Order){
    return new Promise((resolve,reject) => {
        conexionMongo().then(db => {
            db.db("prueba").collection("orders").insertOne(Order, function(err,res){
                db.close();
                if(err){
                    reject(err);                
                }else{
                    resolve({resultado:true, message: res.insertedCount + " rows inserted with ID: " + res.insertedId});        
                }                
            });
        });
    });
}

var updateData = function(query, newVals){
    return new Promise((resolve,reject) => {
        conexionMongo().then(db => {
            db.db("prueba").collection("orders").updateOne(query,newVals, function(err,res){
                db.close();
                if(err){
                    reject(err);
                }else{
                    resolve({resultado:true, message: res.result.nModified + " rows updated"});
                }  
            });
        });
    });
}

var deleteData = function(query){
    return new Promise((resolve, reject) => {
        conexionMongo().then( db => {
            db.db("prueba").collection("orders").deleteOne(query, function(err, res){
                db.close();
                if(err){
                    reject(err);                
                }else{
                    resolve({resultado:true, message: res.result.n + " rows deleted"});
                }               
           });
        });
    });
}

var conexionMongo = function(){
    return new Promise((resolve,reject) => {
        var MongoClient = mongo.MongoClient;
        var url = "mongodb://localhost:27017/prueba"; 
        MongoClient.connect(url, {useNewUrlParser:true}, function(err, db){
            if(err) reject(err);            
            resolve(db);
        });
    });   
}
